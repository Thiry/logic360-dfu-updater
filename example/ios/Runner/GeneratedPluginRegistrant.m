//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"

#if __has_include(<flutter_blue/FlutterBluePlugin.h>)
#import <flutter_blue/FlutterBluePlugin.h>
#else
@import flutter_blue;
#endif

#if __has_include(<flutter_nordic_dfu/FlutterNordicDfuPlugin.h>)
#import <flutter_nordic_dfu/FlutterNordicDfuPlugin.h>
#else
@import flutter_nordic_dfu;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FlutterBluePlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterBluePlugin"]];
  [FlutterNordicDfuPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterNordicDfuPlugin"]];
}

@end
