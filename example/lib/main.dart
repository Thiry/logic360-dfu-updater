import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_nordic_dfu/flutter_nordic_dfu.dart';
import 'package:flutter_blue/flutter_blue.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  StreamSubscription<ScanResult> scanSubscription;
  List<ScanResult> scanResults = <ScanResult>[];
  bool dfuRunning = false;
  bool checkingFirmware = false;
  int dfuProgress;
  String dfuDevice;
  String lastDFUDevice = "";
  String lastFirmware = "";
  String lastFirmwareDevice = "";

  final PrimaryColor = const Color(0xFFFCB103);

  @override
  void initState() {
    super.initState();
    dfuProgress = 0;
    dfuDevice = "";
    lastDFUDevice = "N/A";
    lastFirmware = "N/A";
    lastFirmwareDevice = "N/A";
  }

  Future<void> checkFW(device, context) async {
    stopScan();
    checkingFirmware = true;
    dfuDevice = device.id.id.replaceAll(':','');
    String dfuDeviceName = dfuDevice.substring(dfuDevice.length - 6);

    await device.connect().then((d) async {
      device.discoverServices().then((se) async {
        await se.forEach((s) async {
          if (s.uuid.toString().toUpperCase() == "6E400001-B5A3-F393-E0A9-E50E24DCCA9E") {

            var characteristics = s.characteristics;
            for(BluetoothCharacteristic c in characteristics) {
              if (c.uuid.toString().toUpperCase() == "6E400003-B5A3-F393-E0A9-E50E24DCCA9E") {
                await c.setNotifyValue(true);

                var got_return = false;

                Future.delayed(const Duration(milliseconds: 7500), () {
                  print(got_return);
                  if (!got_return) {
                    device.disconnect();
                    checkingFirmware = false;
                    setState(() => lastFirmware = "Failed Request");
                    setState(() => lastFirmwareDevice = dfuDeviceName);
                    stopScan();
                  }
                });

                c.value.listen((value) async {
                print("return value");
                print(value);
                  if (value.isNotEmpty) {
                    got_return = true;
                    var firmware = value[3].toString() + "." + value[4].toString();

                    device.disconnect();

                    checkingFirmware = false;
                    setState(() => lastFirmware = firmware);
                    setState(() => lastFirmwareDevice = dfuDeviceName);

                    stopScan();
                  }
                });
              }
            }

            for(BluetoothCharacteristic c in characteristics) {
              if (c.uuid.toString().toUpperCase() == "6E400002-B5A3-F393-E0A9-E50E24DCCA9E") {
                //print("Writing MEOW");
                var awrite = await c.write([0x54, 0x00, 0x01, 0xAB]);
              }
            }
          }
        });
      });
    });
  }

  Future<void> doDfu(device) async {
    stopScan();
    dfuRunning = true;
    dfuDevice = device.id.id.replaceAll(':','');
    String dfuDeviceName = device.name;
    lastDFUDevice = device.id.id.replaceAll(':','');
    lastDFUDevice = lastDFUDevice.substring(lastDFUDevice.length - 6);

    try {
      var s = await FlutterNordicDfu.startDfu(
        device.id.id,
        'assets/file.zip',
        fileInAsset: true,
        progressListener:
            DefaultDfuProgressListenerAdapter(onProgressChangedHandle: (
          deviceAddress,
          percent,
          speed,
          avgSpeed,
          currentPart,
          partsTotal,
        ) async {
          //print('deviceAddress: $deviceAddress, percent: $percent');
          setState(() => dfuProgress = percent);
          if (percent == 100) {
            dfuRunning = false;
            setState(() => dfuProgress = -1);

            for (var i = 0; i < scanResults.length; i++) {
              if (scanResults[i].device.id.toString().replaceAll(':','') == dfuDevice) {
                print(scanResults[i].device.id.toString().replaceAll(':',''));
                print(dfuDevice);

                scanResults.remove(i);
              }
            }

            stopScan();
          }
        }),
      );
      //print(s);
      dfuRunning = false;
    } catch (e) {
      dfuRunning = false;
      print(e.toString());
    }
  }

  void startScan() {
    scanSubscription?.cancel();
    setState(() {
      scanResults.clear();
      scanSubscription = flutterBlue.scan().listen(
        (scanResult) {
          if (scanResults.firstWhere(
                  (ele) => ele.device.id == scanResult.device.id,
                  orElse: () => null) !=
              null) {
            return;
          }
          setState(() {
            /// add result to results if not added
            if (scanResult.device.name != null && scanResult.device.name == "IsTr") {
              print(scanResult.device.name);
              print(scanResult.device.id);

              scanResults.add(scanResult);
              scanResults.sort((a, b) => b.rssi.compareTo(a.rssi));
            }
          });
        },
      );
    });
    setState(() => dfuProgress = 0);
  }

  void stopScan() {
    scanSubscription?.cancel();
    scanSubscription = null;
    flutterBlue.stopScan();
    setState(() => scanSubscription = null);
  }

  @override
  Widget build(BuildContext context) {
    final isScanning = scanSubscription != null;
    final hasDevice = scanResults.length > 0;

    if (dfuRunning) {
      return MaterialApp(
        home: new Scaffold(
          appBar: new AppBar(
            title: new Text('Instant-Trace Badge Update'),
            backgroundColor: PrimaryColor,
          ),
          body: new Center(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text('UPDATING FIRMWARE: ' + dfuDevice),
                Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text('DO NOT Turn the Badge off.'),
                new Text('DO NOT Turn the Application off.'),
                Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                (dfuProgress > 0) ? new Text('Updating: ${dfuProgress}%')
                  : new Text('Connecting to Device...'),
                Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                (dfuProgress > 0) ? new LinearProgressIndicator(
                  value: (dfuProgress * 0.01),
                ) : new CircularProgressIndicator()
                ],
            ),
          ),
        ),
      );
    } else if (checkingFirmware) {
      return MaterialApp(
        home: new Scaffold(
          appBar: new AppBar(
            title: new Text('Instant-Trace Badge Update'),
            backgroundColor: PrimaryColor,
          ),
          body: new Center(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text('CHECKING FIRMWARE: ' + dfuDevice),
                Padding(
                  padding: EdgeInsets.all(10.0),
                ),
                new Text('DO NOT Turn the Badge off.'),
                new Text('DO NOT Turn the Application off.')],
            ),
          ),
        ),
      );
    } else {
      return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Instant-Trace Badge Update'),
            actions: <Widget>[
              isScanning
                  ? IconButton(
                      icon: Icon(Icons.pause_circle_filled),
                      onPressed: dfuRunning ? null : stopScan,
                    )
                  : IconButton(
                      icon: Icon(Icons.play_arrow),
                      onPressed: dfuRunning ? null : startScan,
                    )
            ],
            backgroundColor: PrimaryColor,
          ),
          body: !hasDevice
              ? const Center(
                  child: const Text('No device'),
                )
              : new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Text('LAST DEVICE UPDATED: ' + lastDFUDevice, style: TextStyle(fontWeight: FontWeight.bold)),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                    ),
                    new Text('LAST DEVICE CHECKED: ' + lastFirmwareDevice + "  REV: " + lastFirmware, style: TextStyle(fontWeight: FontWeight.bold)),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    Expanded(child: ListView.separated(
                      padding: const EdgeInsets.all(8),
                      itemBuilder: _deviceItemBuilder,
                      separatorBuilder: (context, index) => const SizedBox(height: 5),
                      itemCount: scanResults.length,
                    ))],
                  ))
        ),
      );
    }
  }

  Widget _deviceItemBuilder(BuildContext context, int index) {
    var result = scanResults[index];
    return DeviceItem(
      scanResult: result,
      onPress: dfuRunning
          ? null
          : () async {
              await this.doDfu(result.device);
            },
      onPressFW : dfuRunning
          ? null
          : () async {
              await this.checkFW(result.device, context);
            },
    );
  }
}

class ProgressListenerListener extends DfuProgressListenerAdapter {
  @override
  void onProgressChanged(String deviceAddress, int percent, double speed,
      double avgSpeed, int currentPart, int partsTotal) {
    super.onProgressChanged(
        deviceAddress, percent, speed, avgSpeed, currentPart, partsTotal);
    //print('deviceAddress: $deviceAddress, percent: $percent');
  }
}

class DeviceItem extends StatelessWidget {
  final ScanResult scanResult;

  final VoidCallback onPress;
  final VoidCallback onPressFW;

  DeviceItem({this.scanResult, this.onPress, this.onPressFW});

  @override
  Widget build(BuildContext context) {
    var name = "Unknow";
    if (scanResult.device.name != null && scanResult.device.name.length > 0) {
      name = scanResult.device.id.id.replaceAll(':','');
      name = name.substring(name.length - 6);
    }
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Icon(Icons.bluetooth),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(name, style:TextStyle(fontSize: 24)),
                ],
              ),
            ),
            FlatButton(onPressed: onPress, child: Text("Start Update")),
            FlatButton(onPressed: onPressFW, child: Text("Check Rev"))
          ],
        ),
      ),
    );
  }
}
